<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class ApiController extends Controller
{
    /**
     * @Route("/api/token", name="api_token")
     */
    public function apiToken(Request $request)
    {
        $token = $request->get('token');
        $tokenDecoded = $this->get('lexik_jwt_authentication.encoder')->decode($token);
        unset($tokenDecoded['username']);
        return new JsonResponse(['allServiceInfo' => $tokenDecoded]);
    }

    /**
     * @Route("/api/user", name="api_user")
     */
    public function apiUser(Request $request)
    {
        $token = $request->get('token');
        $tokenDecoded = $this->get('lexik_jwt_authentication.encoder')->decode($token);
        return new JsonResponse(['userInfo' => $tokenDecoded['username']]);
    }

    /**
     * @Route("/api/login", name="api_login")
     */
    public function apiLogin(Request $request)
    {
        $userData = new \stdClass();
        $userData->username = $request->get('username');
        $userData->password = $request->get('password');

        $user = $this->getDoctrine()
            ->getRepository('App:User')
            ->findOneBy(['username' => $userData->username]);

        if (!$user) {
            throw $this->createNotFoundException();
        }
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user,  $userData->password);
        if (!$isValid) {
            throw new BadCredentialsException();
        }

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => time() + 3600 // 3600 1hour expiration
            ]);
        return new JsonResponse(['token' => $token]);
    }

    /**
     * @Route("/api/register", name="api_register")
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function createUserAction(UserPasswordEncoderInterface $encoder )
    {
        $entity = new User('John_'.rand(1,100));
        $entity->setPassword($encoder->encodePassword($entity, 12345));
        $entity->setEmail('mmm'.rand(100, 99999).'@jjj.com.ua');
        $entity->setIsActive(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        return new Response('<html lang="en"><body><h1>Genus '.$entity->getUsername().' was Created!</h1></body></html>');
    }
}
